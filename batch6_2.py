from numpy import *
from array import *
end_point = 105
rows, cols = (250, 250)
dx = array([1, 0, 0, -1])
dy = array([0, 1, -1, 0])
Direction = array(['e', 'n', 's', 'w'])
sta = [0] * 30
num = array([0, 3])

def directin(x, y, pos) :
    global ans, result
    if pos == n + 1 :
        if x == end_point and y == end_point :
            ans = ans + 1
            result += Direction[sta[1]]
            for i in range(2, n + 1):
                result += Direction[sta[i]]

def def(x, y, pos, dir):
    direction(x, y, pos)
    step = 0
    for i in range(pos, n + 1) :
        step += i
    if abs(x - end_point) + abs(y - end_point) > step :
        return 
    if dir == -1 :
        for i in range(4) :
            xx = x + pos * dx[i]
            yy = y + pos * dy[i]
            if is_visited(xx, yy) :
                continue
            k = 0
            if 1 <= i and i < 3 :
                for k in range(min(y, yy ), max(y, yy ) + 2 ) :
                    if gra[xx][k] :
                        break
                if k == max(y, yy ) + 1 :
                    sta[pos] = i
                    mark_as_visited(xx, yy)
                    dfs(xx, yy, pos + 1, 1)
                    mark_as_unvisited(xx, yy)
            else:
                for k in range(min(x, xx), max(x, xx) + 2 ):
                    if gra[k][yy] :
                        break
                if k == max(x, xx ) + 1 :
                    sta[pos] = i
                    mark_as_visited(xx, yy)
                    dfs(xx, yy, pos + 1, 0)
                    mark_as_unvisited(xx, yy)
    else :
        if dir == 0 :
            for i in range(1, 3) :
                xx = x + pos * dx[i]
                yy = y + pos * dy[i]
                if is_visited(xx, yy) :
                    continue
                k = 0
                for k in range(min(y, yy), max(y, yy) + 2 ) :
                    if gra[xx][k] :
                        break
                if k == max(y, yy) + 1 :
                    sta[pos] = i
                    mark_as_visited(xx, yy)
                    dfs(xx, yy, pos + 1, 1)
                    mark_as_unvisited(xx, yy)
        elif dir == 1 :
            for j in range(2) :
                i = num[j]
                xx = x + pos * dx[i]
                yy = y + pos * dy[i]
                if is_visited(xx, yy) :
                    continue
                k = 0
                for k in range(min(x, xx), max(x, xx) + 2) :
                    if gra[k][yy] :
                        break
                if k == max(x, xx) + 1 :
                    sta[pos] = i
                    mark_as_visited(xx, yy)
                    dfs(xx, yy, pos + 1, 0)
                    mark_as_unvisited(xx, yy)
    return ans
